// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDSB5F9In8b2JSQoO8Zr3OFi-7Q1en0qw0",
    authDomain: "loginionic-dec2e.firebaseapp.com",
    databaseURL: "https://loginionic-dec2e.firebaseio.com",
    projectId: "loginionic-dec2e",
    storageBucket: "",
    messagingSenderId: "829235773439",
    appId: "1:829235773439:web:59ff7b67b69b7544b9f1ca"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
